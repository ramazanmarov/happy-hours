package com.example.domain.models

data class Tokens(
    val refresh: String,
    val access: String
)