package com.example.domain.models

data class Location(
    val type: String,
    val coordinates: List<String>
)