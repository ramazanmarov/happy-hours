package com.example.domain.models

data class EstablishmentList(
    val results: List<Establishment>
)

