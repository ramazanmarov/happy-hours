package com.example.domain.models

data class BeverageCategory(
    val id: Int,
    val name: String,
    val beverages: List<String>
)