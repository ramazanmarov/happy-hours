package com.example.domain.models


data class QRCodeResponse(
    val id: Int,
    val qrCodeImage: String
)
