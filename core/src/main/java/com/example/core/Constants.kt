package com.example.core

object Constants {
    object Deeplink {
        const val DEEPLINK_NAV_TO_MAIN_MODULE = "happyhours://main/mainFlowFragment"
        const val DEEPLINK_NAV_TO_AUTH_MODULE = "happyhours://main/authFlowFragment"
    }
    object DatePattern {
        const val ddMMyyyy = "dd/MM/yyyy"
        const val yyyyMMdd = "yyyy-MM-dd"
    }
}