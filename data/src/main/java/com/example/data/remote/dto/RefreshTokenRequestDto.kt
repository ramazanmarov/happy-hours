package com.example.data.remote.dto

data class RefreshTokenRequestDto(
    val refresh: String
)
