package com.example.data.remote.dto

data class LogoutRequestDto(
    val refresh: String
)